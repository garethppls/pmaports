# Maintainer: Alexey Minnekhanov <alexeymin@postmarketos.org>
pkgname=lk2nd
pkgver=0.11.0
pkgrel=0
pkgdesc="Secondary little kernel (lk) bootloader for several Qualcomm devices"
arch="armv7 aarch64"
url="https://github.com/msm8916-mainline/lk2nd"
license="MIT AND GPL-2.0-only"
makedepends="dtc dtc-dev gcc-arm-none-eabi python3 py3-libfdt"
source="$pkgname-$pkgver.tar.gz::https://github.com/msm8916-mainline/lk2nd/archive/refs/tags/$pkgver.tar.gz"
options="!check !archcheck !tracedeps !strip pmb:cross-native"
# This APKBUILD builds bare metal executable; options are very specific!
# * there are no tests (how do you even test the bootloader?);
# * binaries are "arch-independent"; aarch64 build also produces armv7 binary,
#   because ARM CPUs boot in 32-bit mode;
# * there are no depends for bootloader, it's a kernel and OS in one binary;
# * stripping of unused sections is done in lk2nd's own ld script.

subpackages="
	$pkgname-msm8916
	$pkgname-msm8916-appended:msm8916_appended
	$pkgname-msm8974
	$pkgname-msm8226-appended:msm8226_appended
"

build() {
	# This builds baremetal executable; distro-specific compiler flags
	#   and optimizations can't be set by build system.
	unset CFLAGS
	unset CXXFLAGS
	unset CPPFLAGS
	unset LDFLAGS
	unset CROSS_COMPILE
	unset CC
	unset CXX

	# ccache and crossdirect "gcc"s are in PATH, get rid of them too,
	#    redefine PATH to be bare minimum
	unset PATH
	export PATH=/usr/sbin:/usr/bin:/sbin:/bin

	make LK2ND_VERSION="$pkgver-r$pkgrel-postmarketos" TOOLCHAIN_PREFIX=arm-none-eabi- lk2nd-msm8916
	make LK2ND_VERSION="$pkgver-r$pkgrel-postmarketos" TOOLCHAIN_PREFIX=arm-none-eabi- lk2nd-msm8974
	make LK2ND_VERSION="$pkgver-r$pkgrel-postmarketos" TOOLCHAIN_PREFIX=arm-none-eabi- lk2nd-msm8226
}

package() {
	# main package is empty
	mkdir -p "$pkgdir"
}

msm8916() {
	install -Dm644 "$builddir"/build-lk2nd-msm8916/lk2nd.img -t \
		"$subpkgdir"/boot
}

msm8916_appended() {
	install -Dm644 "$builddir"/build-lk2nd-msm8916/lk2nd-appended-dtb.img \
		"$subpkgdir"/boot/lk2nd.img
}

msm8974() {
	install -Dm644 "$builddir"/build-lk2nd-msm8974/lk2nd.img -t \
		"$subpkgdir"/boot
}

msm8226_appended() {
	install -Dm644 "$builddir"/build-lk2nd-msm8226/lk2nd-appended-dtb.img \
		"$subpkgdir"/boot/lk2nd.img
}

sha512sums="
0251d6956596dda496d4f4d6c0ce530650016979f81527fef3242087a24855c972e4b6240ef484b1747a7dcc58b1341eb40d9e5141a29df1fd2a4de18698a578  lk2nd-0.11.0.tar.gz
"
